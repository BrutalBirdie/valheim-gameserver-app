FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

# Install deps
RUN apt-get update && \
    apt-get install -y curl wget file tar bzip2 gzip unzip bsdmainutils python util-linux ca-certificates binutils bc jq tmux netcat lib32gcc1 lib32stdc++6 libsdl2-2.0-0 acl && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Make home of cloudron rwx for lgsm
RUN rm /etc/nginx/sites-enabled/* && \
    rm -rf /var/log/nginx && \
    ln -s /run/nginx/log /var/log/nginx && \
    rm -rf /var/lib/nginx && \
    ln -s /run/nginx/lib /var/lib/nginx && \
    ln -s /run/unity3d /root/.config/unity3d

COPY docker/ /

RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf && \
    sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

CMD [ "/app/code/start.sh" ]
