#!/bin/bash

set -eu

BepInExVersion="5.4.900"

echo "=> Create /app/data/"
mkdir -p /app/data/

echo "=> Create /app/data/mods"
mkdir -p /app/data/mods/

echo "=> Create nginx dirs"
mkdir -p /run/nginx/log /run/nginx/lib

echo "=> Creating unity3d dir"
mkdir -p /run/unity3d/

echo "=> Copy .env"
if [[ ! -f /app/data/.env ]]; then
    cp -v /app/code/.env /app/data/.env
fi

echo "=> Load .env"
set -o allexport; source /app/data/.env; set +o allexport

echo "=> Create Valheim Server"
mkdir -p /run/steamcmd && \
cd /run/steamcmd/ && \
curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - && \
./steamcmd.sh +login anonymous +force_install_dir /run/vhserver-steam/ +app_update 896660 +quit

echo "=> Installing BeepInEx Mod Support if enabled in /app/data/.env"
if [ $MODSUPPORT = 1 ]; then
    cd /tmp/ && \
    wget -O bep.zip https://valheim.thunderstore.io/package/download/denikson/BepInExPack_Valheim/$BepInExVersion/ && \
    unzip -u bep.zip && \
    cp -rv BepInExPack_Valheim/* /run/vhserver-steam/ && \
    mkdir -p /app/data/mods/BepInEx/plugins/ && \
    mkdir -p /app/data/mods/BepInEx/config/
else
    echo "=> Mod Support is disabled - Skipping"
fi

echo "=> Install mods from /app/data/mods/"
if [ $MODSUPPORT = 1 ]; then
    cp -r /app/data/mods/* /run/vhserver-steam/
else
    echo "=> Mod Support is disabled - Skipping."
fi

echo "=> Start Server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon